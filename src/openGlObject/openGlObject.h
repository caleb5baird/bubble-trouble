/*******************************************************************************\
 * OpenGlObject :: A class that draws an openGlObject defined in a json file in
 * assets/opengl-objects.
\*******************************************************************************/
#ifndef OPENGLOBJECT_H_
#define OPENGLOBJECT_H_

#include "../drawable/drawable.h"
#include "../uiDraw/uiDraw.h"
#include <nlohmann/json.hpp>
#include <vector>

using json = nlohmann::json;
using namespace std;
using std::vector;

/*******************************************************************************\
 * Section :: One section of the openGlObject.
\*******************************************************************************/
class Section {
public:
  Section(vector<PT> points, bool hasStroke = false, bool hasFill = false,
          RGB stroke = RGB(), RGB fill = RGB())
      : points(points), stroke(stroke), fill(fill), hasStroke(hasStroke),
        hasFill(hasFill) {}
  Section(Cir circle, bool hasStroke = false, bool hasFill = false,
          RGB stroke = RGB(), RGB fill = RGB())
      : circle(circle), stroke(stroke), fill(fill), hasStroke(hasStroke),
        hasFill(hasFill) {}
  vector<PT> points;
  Cir circle;
  RGB stroke;
  RGB fill;
  bool hasFill;
  bool hasStroke;
  bool isCircle() const { return circle.radius != 0; }
};

/*******************************************************************************\
 * OpenGlObject :: The main class
\*******************************************************************************/
class OpenGlObject : public Drawable {
public:
  OpenGlObject(const char *filename, const Point &position = Point(),
               int rotation = 0, float scale = 1);
  ~OpenGlObject();
  void draw();
  void drawSection(const Section &section);

private:
  const char *filename;
  vector<Section *> sections;
  json parsedJson;
};

#endif // OPENGLOBJECT_H_
