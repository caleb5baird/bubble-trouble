/*******************************************************************************\
 * Source File:
 *    Velocity: stores the velocity of an object.
 * Author:
 *    Caleb Baird
 * Summary:
 *    velocity stored as a dx and a dy, but also accessible as a
 *    direction and magnitude
\*******************************************************************************/

#include "velocity.h"

Velocity::Velocity(float direction, float magnitude, char flag) {
  dx = cosf(deg2Rad direction) * magnitude;
  dy = sinf(deg2Rad direction) * magnitude;
}

// sets the magnitude of the velocity without changing direction
void Velocity::setMagnitude(float magnitude) {
  if (dx == 0.0 && dy == 0.0) {
    dx = sqrtf(magnitude * magnitude / 2);
    dy = sqrtf(magnitude * magnitude / 2);
  } else {
    float direction = getDirection();
    dx = cosf(deg2Rad direction) * magnitude;
    dy = sinf(deg2Rad direction) * magnitude;
  }
}

// sets the direction of the velocity without changing magnitude
void Velocity::setDirection(float direction) {
  float magnitude = getMagnitude();
  dy = sinf(deg2Rad direction) * magnitude;
  dx = cosf(deg2Rad direction) * magnitude;
}

/*******************************************************************************\
 * Velocity :: addMagnitude
 *    Add the given magnitude to the current
 *    magnitude without changing the direction
\*******************************************************************************/
void Velocity::addMagnitude(float magnitude) {
  if (dx == 0 && dy == 0) {
    dx = magnitude;
  } else {
    magnitude += getMagnitude();
    dy = sinf(deg2Rad getDirection()) * magnitude;
    dx = cosf(deg2Rad getDirection()) * magnitude;
  }
}

/*******************************************************************************\
 * Velocity :: addDirection
 *    Add the given direction to the current
 *    direction without changing the magnitude
\*******************************************************************************/
void Velocity::addDirection(float direction) {
  direction += getDirection();
  dy = sinf(deg2Rad direction) * direction;
  dx = cosf(deg2Rad direction) * direction;
}

// Returns the direction of movement based on the dx and dy.
// float Velocity::getDirection()
//{
//    int quadrant = 0;
//    //If Dx or Dy is 0 return the correct value
//    if (dx == 0)
//       if (dy > 0) return 90;
//       else return 270;
//    else if (dy == 0)
//       if (dx > 0) return 0;
//       else return 180;
//    //If not returned find out what quadrant we are in
//     else if (dx > 0 && dy > 0)
//       quadrant = 1;
//    else if (dx < 0 && dy > 0)
//       quadrant = 2;
//    else if (dx < 0 && dy < 0)
//       quadrant = 3;
//    else if (dx > 0 && dy < 0)
//       quadrant = 4;
//
//    int angle = rad2Deg atan(fabs(dy/dx));
//    switch (quadrant)
//    {
//       case 1:
//          return angle;
//       case 2:
//          return angle + 90;
//       case 3:
//          return angle + 180;
//       case 4:
//          return angle + 270;
//       default: return 0;
//     }
// }

/*******************************************************************************\
 * Velocity::Insertion :: Display components on the screen
\*******************************************************************************/
std::ostream &operator<<(std::ostream &out, const Velocity &vel) {
  out << "(" << vel.getDx() << ", " << vel.getDy() << ")";
  return out;
}

/*******************************************************************************\
 * Velocity::extraction :: Prompt for components
\*******************************************************************************/
std::istream &operator>>(std::istream &in, Velocity &vel) {
  float x;
  float y;
  in >> x >> y;

  vel.setDx(x);
  vel.setDy(y);

  return in;
}
