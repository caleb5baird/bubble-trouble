/*******************************************************************************\
 * Header File:
 *    Velocity: stores the velocity of an object.
 * Author:
 *    Caleb Baird
 * Summary:
 *    velocity stored as a dx and a dy, but also accessible as a
 *    direction and magnitude
\*******************************************************************************/

#ifndef VELOCITY_H
#define VELOCITY_H

#ifndef PI
#define PI 3.141592
#endif
#define deg2Rad PI / 180 *
#define rad2Deg 180 / PI *

#include <cmath>
#include <iostream>

/*******************************************************************************\
 * POINT
 * A single position.
\*******************************************************************************/
class Velocity {
private:
  float dx;
  float dy;

public:
  // constructors
  Velocity() : dx(0.0), dy(0.0) {}
  Velocity(float dx, float dy) : dx(dx), dy(dy){};
  Velocity(float direction, float magnitude, char flag);
  Velocity(const Velocity &v) : dx(v.dx), dy(v.dy) {}

  // getters
  float getDx() const { return dx; }
  float getDy() const { return dy; }
  float getMagnitude() { return sqrt(dx * dx + dy * dy); }
  float getDirection() { return tan(dy / dx); }

  // setters
  void setDx(float dx) { this->dx = dx; }
  void setDy(float dy) { this->dy = dy; }
  void setMagnitude(float value);
  void setDirection(float value);

  // Math
  void addDx(float dx) { this->dx += dx; };
  void addDy(float dy) { this->dy += dy; };
  void addMagnitude(float magnitude);
  void addDirection(float direction);

  // Operators so we can do opporations like addition and subtraction on
  // velocities
  Velocity operator+(Velocity const &rs) {
    return Velocity(dx + rs.getDx(), dy + rs.getDy());
  };
  Velocity operator-(Velocity const &rs) {
    return Velocity(dx - rs.getDx(), dy - rs.getDy());
  };
  Velocity operator+=(Velocity &rs) {
    setDx(dx + rs.getDx());
    setDy(dy + rs.getDy());
    return *this;
  };
  Velocity operator-=(Velocity &rs) {
    setDx(dx - rs.getDx());
    setDy(dy - rs.getDy());
    return *this;
  };
};

// stream I/O useful for debugging
std::ostream &operator<<(std::ostream &out, const Velocity &vel);
std::istream &operator>>(std::istream &in, Velocity &vel);

#endif // POINT_H:w
