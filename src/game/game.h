/*******************************************************************************\
 * File: game.h
 * Description: Contains the implementations of the method bodies for the game
 * class.
\*******************************************************************************/

#ifndef GAME_H
#define GAME_H

#include "../bubble/bubble.h"
#include "../cat/cat.h"
#include "../openGlObject/openGlObject.h"
#include "../point/point.h"
#include "../settings/settings.h"
#include "../uiDraw/uiDraw.h"
#include "../uiInteract/uiInteract.h"
#include <list>

/*******************************************************************************\
 * GAME
 * The main game class containing all the state
 *******************************************************************************/
class Game {
public:
  /*****************************************************************************\
   * Constructor Initializes the game
  \*****************************************************************************/
  Game(Point tl, Point br);

  /*****************************************************************************\
   * GAME DESTRUCTOR :: Delete any objects with allocated memory
  \*****************************************************************************/
  ~Game();

  /*****************************************************************************\
   * HandleInput :: Takes actions according to whatever keys the user has
   * pressed.
  \*****************************************************************************/
  void handleInput(const Interface &ui);
  // This takes care of the player movement seperated out to minimize
  // handleInput
  void handlePlayerMovementInput(const Interface &ui);

  /*****************************************************************************\
   * Advance :: Move everything forward one step in time.
  \*****************************************************************************/
  void advance();

  /*****************************************************************************\
   * Draw :: draws everything for the game.
  \*****************************************************************************/
  void draw(const Interface &ui);

private:
  /*****************************************************************************\
   * ApplyGravity :: Applies gravity to all the bubbles
  \*****************************************************************************/
  void applyGravity();

  // The coordinates of the screen
  Point topLeft;
  Point bottomRight;
  Cat *player1;
  int level;

  std::list<Bubble *> bubbles;
  Settings settings;
  OpenGlObject gamePad;
};

#endif /* GAME_H */
