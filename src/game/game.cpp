/*******************************************************************************\
 * File: game.cpp
 *
 * Description: Contains the implementations of the method bodies for the game
 * class.
\*******************************************************************************/
#include "game.h"
#include "../bubble/bubble.h"
#include "../constants.h"
#include "../point/point.h"
#include "../uiDraw/uiDraw.h"
#include "../uiInteract/uiInteract.h"

using namespace std;

/*******************************************************************************\
 * GAME::Constructor :: Setup the initial state of the game
\*******************************************************************************/
Game::Game(Point tl, Point br)
    : topLeft(tl), bottomRight(br),
      gamePad(OpenGlObject("game-controller-arcade.json")),
      settings(Settings()), level(1) {

  player1 = new Cat(Point(0, bottomRight.getY() + FLOOR_HEIGHT));

  bubbles.push_back(new Bubble(Point(tl.getX() + 100, 0),
                               Velocity(1, this->settings.bubbleDx), 3, 300));
};

/*****************************************************************************\
 * GAME DESTRUCTOR :: Delete any objects with allocated memory
\*****************************************************************************/
Game::~Game() {
  delete this->player1;
  for (Bubble *b : this->bubbles) {
    delete b;
  }
}

/*****************************************************************************\
 * HandlePlayerMovementInput :: split out to keep  handleInput small.
\*****************************************************************************/
void Game::handlePlayerMovementInput(const Interface &ui) {

  if (ui.isLeft()) {
    // if I am not going to hit the  left edge, move left at player speed
    if (player1->getPosition().getX() > topLeft.getX() + 40) {
      player1->setVelocityX(-1 * this->settings.playerSpeed);

    } else {
      // player speed would take me past the edge so go just fast enough to
      // reach the edge
      player1->setVelocityX(topLeft.getX() - player1->getPosition().getX() +
                            40);
    }
  } else if (ui.isRight()) {
    // if I am not not going to hit the right edge, move right at player speed
    if (player1->getPosition().getX() < bottomRight.getX()) {
      player1->setVelocityX(this->settings.playerSpeed);
    } else {
      // player speed would take me past the edge so go just fast enough to
      // reach the edge
      player1->setVelocityX(bottomRight.getX() - player1->getPosition().getX());
    }

  } else {
    player1->setVelocityX(0);
  }
}

/*******************************************************************************\
 * GAME::HandleInput :: accept input from the user
\*******************************************************************************/
void Game::handleInput(const Interface &ui) { handlePlayerMovementInput(ui); }

/*******************************************************************************\
 * GAME::Draw :: draws everything for the game.
\*******************************************************************************/
void Game::draw(const Interface &ui) {
  const float gameWidth = bottomRight.getX() - topLeft.getX();
  const float gameHeight = topLeft.getY() - bottomRight.getY();

  drawRect(Point(0, 0), gameWidth, gameHeight, 0, RGB(100, 100, 100), true);
  Point bCenter = Point(topLeft.getX() + gameWidth / 2, bottomRight.getY());
  drawFooter(bCenter, gameWidth - 2, FLOOR_HEIGHT, this->level);

  player1->draw();
  for (Bubble *b : bubbles) {
    b->draw();
  }
}

/*****************************************************************************\
 * CalculateBounceDy :: Given a max height and gravity it calculates the y
 *   portion of the velocity required to bounce up to max height
\*****************************************************************************/
float calculateBounceDy(float maxHeight, float gravity) {
  return sqrt(2 * gravity * maxHeight);
}

/*****************************************************************************\
 * Game::Advance :: Move everything forward one step in time.
\*****************************************************************************/
void Game::advance() {
  this->applyGravity();

  player1->advance();
  for (Bubble *b : bubbles) {
    // if the ball hits the floor then bounce it
    if (b->getPosition().getY() <
        bottomRight.getY() + FLOOR_HEIGHT + b->getRadius()) {
      Velocity tempVelocity = b->getVelocity();
      tempVelocity.setDy(
          calculateBounceDy(b->getBounceHeight(), settings.gravity));
      b->setVelocity(tempVelocity);
    }

    // if the ball hits the wall reverse the dx
    if (b->getPosition().getX() < topLeft.getX() + b->getRadius() ||
        b->getPosition().getX() > bottomRight.getX() - b->getRadius()) {
      Velocity tempVelocity = b->getVelocity();
      tempVelocity.setDx(-1 * tempVelocity.getDx());
      b->setVelocity(tempVelocity);
    }

    b->advance();
  }
};

/*****************************************************************************\
 * Game::ApplyGravity :: Applies gravity to all the bubbles
\*****************************************************************************/
void Game::applyGravity() {
  Velocity tempVelocity;
  for (Bubble *b : bubbles) {
    tempVelocity = b->getVelocity();
    tempVelocity.addDy(-1 * this->settings.gravity);
    b->setVelocity(tempVelocity);
  }
};
