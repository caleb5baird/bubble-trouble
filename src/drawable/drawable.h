#ifndef DRAWABLE_H_
#define DRAWABLE_H_

#include "../point/point.h"

class Drawable {
public:
  Drawable() {}
  Drawable(const Point &position, int rotation = 0, float scale = 1)
      : position(position), rotation(rotation), scale(scale){};

  // Setters
  void setPosition(const Point &position) { this->position = position; }
  void setRotation(const int &rotation) { this->rotation = rotation; }
  void setScale(const float &scale) { this->scale = scale; }

  // Getters
  Point getPosition() const { return position; }
  int getRotation() const { return rotation; }
  float getScale() const { return scale; }

  virtual void Draw(){};

protected:
  Point position;
  int rotation;
  float scale;
};

#endif // DRAWABLE_H_
