#include "../settings/settings.h"
#include <fstream>
#include <iostream>
#include <nlohmann/json.hpp>

using json = nlohmann::json;

/*****************************************************************************\
 * Settings::ResetAll :: Reset all settings to their default values
\*****************************************************************************/
void Settings::resetAll() {
  this->bubbleDx = this->defaultBubbleDx;
  this->gravity = this->defaultGravity;
  this->playerSpeed = this->defaultPlayerSpeed;
}

/*****************************************************************************\
 * Settings::LoadFromFile :: Load the settings from the settings.json file
\*****************************************************************************/
void Settings::loadFromFile() {
  std::fstream f("src/settings.json");
  if (f.fail()) {
    std::cerr << "Failed to open settings.json" << std::endl;
  } else {
    this->settings = json::parse(f);

    // Update the current values with the settings object
    this->bubbleDx = (float)settings["bubble"]["dx"];
    this->gravity = (float)settings["gravity"];
    this->playerSpeed = (float)settings["player"]["speed"];
  }
}

/*****************************************************************************\
 * Settings::WriteToFile :: Write the settings to the settings.json file
\*****************************************************************************/
void Settings::writeToFile() {
  // Update the settings object with the current values
  this->settings["bubble"]["dx"] = this->bubbleDx;
  this->settings["gravity"] = this->gravity;
  this->settings["player"]["speed"] = this->playerSpeed;

  // Write the settings object to the file
  std::ofstream f("src/settings.json");
  if (f.fail()) {
    std::cerr << "Failed to open settings.json" << std::endl;
  } else {
    f << this->settings.dump(2);
  }
}
