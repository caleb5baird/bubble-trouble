/*******************************************************************************\
 * File: movingObject.cpp
 * Description: A moving object is a thing that can be drawn on the screen.
 * It has a position and a velocity and can be alive or dead.
\*******************************************************************************/

#include "movingObject.h"
