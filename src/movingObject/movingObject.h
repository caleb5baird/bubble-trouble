/*******************************************************************************\
 * File: movingObject.h
 * Description: A moving object is a thing that can be drawn on the screen.
 * It has a position and a velocity and can be alive or dead.
\*******************************************************************************/

#ifndef movingObject_h
#define movingObject_h

#include "../drawable/drawable.h"
#include "../uiDraw/uiDraw.h"
#include "../velocity/velocity.h"
#include <stdio.h>

class MovingObject : public Drawable {
protected:
  Velocity velocity;
  bool alive;

public:
  MovingObject(bool alive = true) : alive(alive){};
  MovingObject(const Point &pos = Point(), const Velocity &vel = Velocity(),
               bool alive = true)
      : alive(alive), Drawable(pos), velocity(vel){};

  // getters
  Velocity getVelocity() { return velocity; };
  bool isAlive() { return alive; };
  float getSlope() { return position.getY() / position.getX(); };
  float getYIntercept() {
    return position.getY() - getSlope() * position.getX();
  };
  virtual Point getPreviousPosition() {
    return Point(position.getX() - velocity.getDx(),
                 position.getY() - velocity.getDy());
  };

  // setters
  void kill() { alive = false; };
  virtual void setVelocity(Velocity vel) { velocity = vel; };
  void addVelocity(Velocity value) { velocity += value; };

  // advace the position of the object
  virtual void advance() {
    position.addX(velocity.getDx());
    position.addY(velocity.getDy());
  };

  // draw the object
  virtual void draw() = 0;
};

#endif /* movingObject_h */
