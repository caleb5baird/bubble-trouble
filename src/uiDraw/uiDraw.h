/*******************************************************************************\
 * Header File:
 *    User Interface Draw : put pixels on the screen
 * Summary:
 *    This is the code necessary to draw on the screen. We have a collection
 *    of procedural functions here because each draw function does not
 *    retain state. In other words, they are verbs (functions), not nouns
 *    (variables) or a mixture (objects)
\*******************************************************************************/

#ifndef UI_DRAW_H
#define UI_DRAW_H

#include "../point/point.h" // Where things are drawn
#include <cmath>            // for M_PI, sin() and cos()
#include <string>           // To display text on the screen
#include <vector>
using std::string;
using std::vector;

class PT {
public:
  PT(float x, float y) : x(x), y(y) {}
  PT(const PT &pt) : x(pt.x), y(pt.y) {}
  float x;
  float y;
};

struct RGB {
public:
  RGB() : r(255), g(255), b(255) {}
  RGB(int red, int green, int blue) : r(red), g(green), b(blue) {}
  int r;
  int g;
  int b;
};

struct Cir {
  Cir(PT center, float radius) : center(center), radius(radius) {}
  Cir() : center(0, 0), radius(0) {}
  PT center;
  float radius;
};

/*******************************************************************************\
 * DRAW DIGIT
 * Draw a single digit in the old school line drawing style.  The
 * size of the glyph is 8x11 or x+(0..7), y+(0..10)
\*******************************************************************************/
void drawDigit(const Point &topLeft, char digit, RGB rgb = RGB(),
               float scale = 1);

/*******************************************************************************\
 * DRAW NUMBER
 * Display an integer on the screen using the 7-segment method
\*******************************************************************************/
void drawNumber(const Point &topLeft, int number, RGB rgb = RGB(),
                float scale = 1);

/*******************************************************************************\
 * DRAW TEXT
 * Draw text using a simple bitmap font
\*******************************************************************************/
void drawText(const Point &topLeft, const char *text, RGB rgb = RGB());

/*******************************************************************************\
 * ROTATE
 * Rotate a given point (point) around a given origin (center) by a given
 * number of degrees (angle).
\*******************************************************************************/
void rotate(Point &point, const Point &origin, int rotation = 0);

/*******************************************************************************\
 * DRAW RECTANGLE
 * Draw a rectangle on the screen centered on a given point (center) of
 * a given size (width, height), and at a given orientation (rotation)
 * measured in degrees (0 - 360)
\*******************************************************************************/
void drawRect(const Point &center, int width, int height, int rotation = 0,
              RGB rgb = RGB(), bool fill = false);

/*******************************************************************************\
 * DRAW CIRCLE
 * Draw a circle from a given location (center) of a given size (radius).
\*******************************************************************************/
void drawCircle(const Point &center, int radius, RGB rgb = RGB(),
                bool fill = false);

/*******************************************************************************\
 * DRAW POLYGON
 * Draw a polygon from a given location (center) of a given size (radius).
\*******************************************************************************/
void drawPolygon(const Point &center, int radius = 20, int sides = 3,
                 int rotation = 0, RGB rgb = RGB(), bool fill = false);

/*******************************************************************************\
 * DRAW LINE
 * Draw a line on the screen from the beginning to the end.
\*******************************************************************************/
void drawLine(const Point &begin, const Point &end, RGB rgb = RGB(),
              float thickness = 1);

/*******************************************************************************\
 * DRAW DASHED LINE
 * Draw a dashed line on the screen from the beginning to the end.
\*******************************************************************************/
void drawDashedLine(const Point &begin, const Point &end,
                    const float dashLength, const float dashSeparation,
                    RGB rgb = RGB(), float thickness = 1);

/*******************************************************************************\
 * DRAW DOT
 * Draw a single point on the screen, 2 pixels by 2 pixels
\******************************************************************************/
void drawDot(const Point &point, RGB rgb = RGB());

/*******************************************************************************\
 * DrawFooter :: draws the game footer. This will contain the score, lives,
 *   time, and current level.
\*******************************************************************************/
void drawFooter(const Point &bottomCenter, const float width,
                const float height, const int level);

#endif // UI_DRAW_H
