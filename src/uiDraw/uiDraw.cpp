/*******************************************************************************\
 * Source File:
 *    User Interface Draw : put pixels on the screen
 * Summary:
 *    This is the code necessary to draw on the screen. We have a collection
 *    of procedural functions here because each draw function does not
 *    retain state. In other words, they are verbs (functions), not nouns
 *    (variables) or a mixture (objects)
\*******************************************************************************/

#include <cassert>
#include <sstream> // convert an integer into text
#include <string>  // need you ask?
#include <time.h>  // for clock

#ifdef __APPLE__
#define GL_SILENCE_DEPRECATION
#include <GLUT/glut.h> // Second OpenGL library
#include <openGL/gl.h> // Main OpenGL library
#endif                 // __APPLE__

#ifdef __linux__
#include <GL/gl.h>   // Main OpenGL library
#include <GL/glut.h> // Second OpenGL library
#endif               // __linux__

#ifdef _WIN32
#include <GL/glut.h> // OpenGL library we copied
#include <stdio.h>
#include <stdlib.h>
#define _USE_MATH_DEFINES
#include <cmath>
#include <math.h>
#endif // _WIN32

#include "../point/point.h"
#include "uiDraw.h"
#include <fstream>

#define deg2rad(value) ((M_PI / 180) * (value))

/*******************************************************************************\
 * NumberOutlines :: We are drawing the text for score and things like that by
 *   hand to make it look "old school." These are how we render each individual
 *   charactger. Note how -1 indicates "done".  These are paired coordinates
 *   where the even are the x and the odd are the y and every 2 pairs represents
 *   a point
\*******************************************************************************/
const char NUMBER_OUTLINES[10][20] = {
    {0, 0, 7, 0, 7, 0, 7, 10, 7, 10, 0, 10, 0, 10, 0, 0, -1, -1, -1, -1}, // 0
    {7,  0,  7,  10, -1, -1, -1, -1, -1, -1,
     -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},                              // 1
    {0, 0, 7, 0, 7, 0, 7, 5, 7, 5, 0, 5, 0, 5, 0, 10, 0, 10, 7, 10},       // 2
    {0, 0, 7, 0, 7, 0, 7, 10, 7, 10, 0, 10, 4, 5, 7, 5, -1, -1, -1, -1},   // 3
    {0, 0, 0, 5, 0, 5, 7, 5, 7, 0, 7, 10, -1, -1, -1, -1, -1, -1, -1, -1}, // 4
    {7, 0, 0, 0, 0, 0, 0, 5, 0, 5, 7, 5, 7, 5, 7, 10, 7, 10, 0, 10},       // 5
    {7, 0, 0, 0, 0, 0, 0, 10, 0, 10, 7, 10, 7, 10, 7, 5, 7, 5, 0, 5},      // 6
    {0,  0,  7,  0,  7,  0,  7,  10, -1, -1,
     -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},                         // 7
    {0, 0, 7, 0, 0, 5, 7, 5, 0, 10, 7, 10, 0, 0, 0, 10, 7, 0, 7, 10}, // 8
    {0, 0, 7, 0, 7, 0, 7, 10, 0, 0, 0, 5, 0, 5, 7, 5, -1, -1, -1, -1} // 9
};

/*******************************************************************************\
 * DrawDigit :: * Draw a single digit in the old school line drawing style.
 *   The size of the glyph is 8x11 or x+(0..7), y+(0..10)
 * INPUT topLeft : The top left corner of the character
 *         digit : The digit we are rendering: '0' .. '9'
\*******************************************************************************/
void drawDigit(const Point &topLeft, char digit, RGB rgb, float scale) {
  // we better be only drawing digits
  assert(isdigit(digit));
  if (!isdigit(digit))
    return;

  // compute the row as specified by the digit
  int r = digit - '0';
  assert(r >= 0 && r <= 9);

  // go through each segment.
  for (int c = 0; c < 20 && NUMBER_OUTLINES[r][c] != -1; c += 4) {
    assert(NUMBER_OUTLINES[r][c] != -1 && NUMBER_OUTLINES[r][c + 1] != -1 &&
           NUMBER_OUTLINES[r][c + 2] != -1 && NUMBER_OUTLINES[r][c + 3] != -1);

    // Draw a line based off of the num structure for each number
    Point start;
    start.setX((topLeft.getX() + NUMBER_OUTLINES[r][c]) * scale);
    start.setY((topLeft.getY() - NUMBER_OUTLINES[r][c + 1]) * scale);
    Point end;
    end.setX((topLeft.getX() + NUMBER_OUTLINES[r][c + 2]) * scale);
    end.setY((topLeft.getY() - NUMBER_OUTLINES[r][c + 3]) * scale);

    drawLine(start, end, rgb, scale);
  }
}

/*******************************************************************************\
 * DrawNumber :: Display an integer on the screen using the 7-segment method
 * INPUT topLeft : The top left corner of the character
 *         digit : The digit we are rendering: '0' .. '9'
\*******************************************************************************/
void drawNumber(const Point &topLeft, int number, RGB rgb, float scale) {
  // our cursor, if you will. It will advance as we output digits
  Point point = topLeft;

  // is this negative
  bool isNegative = (number < 0);
  number *= (isNegative ? -1 : 1);

  // render the number as text
  std::ostringstream sout;
  sout << number;
  string text = sout.str();

  // handle the negative
  if (isNegative) {
    glBegin(GL_LINES);
    glColor3ub(rgb.r, rgb.g, rgb.b);
    glVertex2f((point.getX() + 1) * scale, (point.getY() - 5) * scale);
    glVertex2f((point.getX() + 5) * scale, (point.getY() - 5) * scale);
    glColor3ub(255, 255, 255); // reset to white
    glLineWidth(scale);
    glEnd();
    point.addX(11);
  }

  // walk through the text one digit at a time
  for (const char *p = text.c_str(); *p; p++) {
    assert(isdigit(*p));
    drawDigit(point, *p, rgb, scale);
    point.addX(11);
  }
}

/*******************************************************************************\
 * DrawText :: Draw text using a simple bitmap font
 * INPUT topLeft : The top left corner of the text
 *          text : The text to be displayed
\*******************************************************************************/
void drawText(const Point &topLeft, const char *text, RGB rgb) {
  void *pFont = GLUT_BITMAP_TIMES_ROMAN_24;

  glColor3ub(rgb.r, rgb.g, rgb.b);
  // prepare to draw the text from the top-left corner
  glRasterPos2f(topLeft.getX(), topLeft.getY());

  // loop through the text
  for (const char *p = text; *p; p++)
    glutBitmapCharacter(pFont, *p);
  glColor3ub(255, 255, 255); // reset to white
}

/*******************************************************************************\
 * DrawPolygon :: Draw a POLYGON from a given location (center) of a given size
 *   (radius).
 *  INPUT center : Center of the polygon
 *        radius : Size of the polygon
 *        points : How many points will we draw it.  Larger the number, the more
 *                 line segments we will use
 *      rotation : True circles are rotation independent. However, if you are
 *                 drawing a 3-sided polygon (triangle), this matters!
\*******************************************************************************/
void drawPolygon(const Point &center, int radius, int sides, int rotation,
                 RGB rgb, bool fill) {
  // begin drawing
  glBegin(fill ? GL_POLYGON : GL_LINE_LOOP);
  glColor3ub(rgb.r, rgb.g, rgb.b);

  // loop around a circle the given number of times drawing a line from
  // one point to the next
  for (double i = 0; i < 2 * M_PI; i += (2 * M_PI) / sides) {
    Point temp(center.getX() + (radius * cos(i)),
               center.getY() + (radius * sin(i)));
    rotate(temp, center, rotation);
    glVertex2f(temp.getX(), temp.getY());
  }

  glColor3ub(255, 255, 255); // reset to white
  glEnd();
}

/*******************************************************************************\
 * Rotate :: Rotate a given point (point) around a given origin (center) by a
 *           given number of degrees (angle).
 * INPUT point : The point to be moved
 *      center : The center point we will rotate around
 *    rotation : Rotation in degrees
 * OUTPUT point : The new position
\*******************************************************************************/
void rotate(Point &point, const Point &origin, int rotation) {
  // because sine and cosine are expensive, we want to call them only once
  double cosA = cos(deg2rad(rotation));
  double sinA = sin(deg2rad(rotation));

  // remember our original point
  Point tmp(false /*check*/);
  tmp.setX(point.getX() - origin.getX());
  tmp.setY(point.getY() - origin.getY());

  // find the new values
  point.setX(static_cast<int>(tmp.getX() * cosA - tmp.getY() * sinA) +
             origin.getX());
  point.setY(static_cast<int>(tmp.getX() * sinA + tmp.getY() * cosA) +
             origin.getY());
}

/*******************************************************************************\
 * DrawLine :: Draw a line on the screen from the beginning to the end.
 * INPUT begin : The position of the beginning of the line
 *         end : The position of the end of the line
\*******************************************************************************/
void drawLine(const Point &begin, const Point &end, RGB rgb, float thickness) {
  glLineWidth(thickness);
  glBegin(GL_LINES);
  glColor3ub(rgb.r, rgb.g, rgb.b);

  // Draw the actual line
  glVertex2f(begin.getX(), begin.getY());
  glVertex2f(end.getX(), end.getY());

  glColor3ub(255, 255, 255); // reset to white
  glLineWidth(thickness);
  glEnd();
}

/*******************************************************************************\
 * DrawDashedLine :: Draw a dashed line on the screen from the beginning to the
 *   end.
 * INPUT     begin : The position of the beggingin of the line.
 *             end : The position of the end of the line.
 *      dashLength : The length of the line segments.
 *  dashSeparation : The distance between line segments.
\*******************************************************************************/
void drawDashedLine(const Point &begin, const Point &end,
                    const float dashLength, const float dashSeparation, RGB rgb,
                    float thickness) {
  glLineWidth(thickness);
  glBegin(GL_LINES);
  glColor3ub(rgb.r, rgb.g, rgb.b);

  float lineRise = end.getY() - begin.getY();
  float lineRun = end.getX() - begin.getX();
  float lineLength = sqrt(lineRise * lineRise + lineRun * lineRun);
  float dashRise = lineRise * dashLength / lineLength;
  float dashRun = lineRun * dashLength / lineLength;
  float x = begin.getX();
  float y = begin.getY();
  float spaceRise = lineRise * dashSeparation / lineLength;
  float spaceRun = lineRun * dashSeparation / lineLength;

  // Draw the actual lines
  float dashedLineLength = 0;
  while (dashedLineLength < lineLength) {
    glVertex2f(x, y);
    glVertex2f(x += dashRun, y += dashRise);
    x += spaceRun;
    y += spaceRise;
    dashedLineLength += dashLength + dashSeparation;
  }

  // Complete drawing
  glColor3ub(255, 255, 255); // reset to white
  glEnd();
}

/*******************************************************************************\
 * DrawRectangle :: Draw a rectangle on the screen centered on a given point
 *   (center) of a given size (width, height), and at a given orientation
 *   (rotation)
 * INPUT center : Center of the rectangle width     Horizontal size
 *       height : Vertical size
 *     rotation : Orientation
\*******************************************************************************/
void drawRect(const Point &center, int width, int height, int rotation, RGB rgb,
              bool fill) {
  Point tl(center.getX() - ((float)width / 2),
           center.getY() + ((float)height / 2)); // top left
  Point tr(center.getX() + ((float)width / 2),
           center.getY() + ((float)height / 2)); // top right
  Point bl(center.getX() - ((float)width / 2),
           center.getY() - ((float)height / 2)); // bottom left
  Point br(center.getX() + ((float)width / 2),
           center.getY() - ((float)height / 2)); // bottom right

  // Rotate all points the given degrees
  rotate(tl, center, rotation);
  rotate(tr, center, rotation);
  rotate(bl, center, rotation);
  rotate(br, center, rotation);

  // Finally draw the rectangle
  glBegin(fill ? GL_POLYGON : GL_LINE_STRIP);
  glColor3ub(rgb.r, rgb.g, rgb.b);
  glVertex2f(tl.getX(), tl.getY());
  glVertex2f(tr.getX(), tr.getY());
  glVertex2f(br.getX(), br.getY());
  glVertex2f(bl.getX(), bl.getY());
  glVertex2f(tl.getX(), tl.getY());
  glColor3ub(255, 255, 255); // reset to white
  glEnd();
}

/*******************************************************************************\
 * DrawCircle :: Draw a circle from a given location (center) of a given size
 *   (radius).
 * INPUT center : Center of the circle radius   Size of the circle
\*******************************************************************************/
void drawCircle(const Point &center, int radius, RGB rgb, bool fill) {
  //   assert(radius > 1.0);
  const double increment = 0.1 / (double)radius;

  // begin drawing
  glBegin(fill ? GL_POLYGON : GL_LINE_LOOP);
  glColor3ub(rgb.r, rgb.g, rgb.b);

  // go around the circle
  for (double radians = 0; radians < M_PI * 2.0; radians += increment)
    glVertex2f(center.getX() + (radius * cos(radians)),
               center.getY() + (radius * sin(radians)));

  // complete drawing
  glColor3ub(255, 255, 255); // reset to white
  glEnd();
}

/*******************************************************************************\
 * DrawDot :: Draw a single point on the screen, 2 pixels by 2 pixels
 * INPUT point : The position of the dow
\*******************************************************************************/
void drawDot(const Point &point, RGB rgb) {
  glBegin(GL_POINTS);
  glColor3ub(rgb.r, rgb.g, rgb.b);
  glVertex2f(point.getX(), point.getY());
  glVertex2f(point.getX() + 1, point.getY());
  glVertex2f(point.getX() + 1, point.getY() + 1);
  glVertex2f(point.getX(), point.getY() + 1);
  glColor3ub(255, 255, 255); // reset to white
  glEnd();
}

/*******************************************************************************\
 * DrawFooter :: draws the game footer. This will contain the score, lives,
 *   time, and current level.
\*******************************************************************************/
void drawFooter(const Point &bottomCenter, const float width,
                const float height, const int level) {
  // Draw the footer background
  drawRect(Point(bottomCenter.getX(), bottomCenter.getY() + height / 2), width,
           height, 0, RGB(153, 153, 153), true);

  // Draw the time bar background
  drawRect(Point(bottomCenter.getX(), bottomCenter.getY() + height / 1.3),
           width - 10, 30, 0, RGB(102, 102, 102), true);
  // Draw the time bar
  drawRect(Point(bottomCenter.getX(), bottomCenter.getY() + height / 1.3),
           width - 10, 30, 0, RGB(153, 26, 26), true);

  // Draw the level indicator
  drawRect(Point(bottomCenter.getX() - 20, bottomCenter.getY() + height / 2),
           100, 40, 0, RGB(255, 255, 255), true);
  drawRect(Point(bottomCenter.getX() - 20, bottomCenter.getY() + height / 4),
           50, 40, 0, RGB(255, 255, 255), true);
  Point textPosition = Point(bottomCenter);
  textPosition.addY(65);
  textPosition.addX(-45);
  drawText(textPosition, "Level", RGB(255, 128, 0));
  textPosition.addY(-35);
  textPosition.addX(20);
  char levelChar = '0' + level;
  drawText(textPosition, &levelChar, RGB(255, 128, 0));
}
