#include "cat.h"
#include "../uiDraw/uiDraw.h"

void Cat::draw() { cat.draw(); }

void Cat::advance() {
  MovingObject::advance();
  cat.setPosition(Point(position.getX(), position.getY() + 45));
}
