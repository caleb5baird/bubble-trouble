#ifndef CAT_H
#define CAT_H

#include "../movingObject/movingObject.h"
#include "../openGlObject/openGlObject.h"

class Cat : public MovingObject {
public:
  Cat(const Point &position)
      : MovingObject(position),
        cat("cat.json", Point(position.getX(), position.getY() + 45), 0, .7){};

  virtual void draw();

  void setVelocityX(float dx) { this->velocity.setDx(dx); };

  virtual void advance();

  /*****************************************************************************\
   * SetVelocity :: Cats can only move left and right, so we only need to set
   * the dx
  \*****************************************************************************/
  virtual void setVelocity(float &dx) { this->velocity = Velocity(dx, 0); };
  virtual void setVelocity(const Velocity &v) {
    this->velocity = Velocity(v.getDx(), 0);
  };

private:
  OpenGlObject cat;
};

#endif // CAT_H
