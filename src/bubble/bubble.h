#ifndef BUBBLE_H_
#define BUBBLE_H_

#include "../movingObject/movingObject.h"

class Bubble : public MovingObject {
public:
  void draw() { drawCircle(this->position, size * 8, color, true); }
  Bubble(const Point &point = Point(), const Velocity &velocity = Velocity(),
         int size = 1, float bounceHeight = 100, RGB color = RGB(255, 255, 0))
      : MovingObject(point, velocity), size(size), color(color),
        bounceHeight(bounceHeight){};

  float getRadius() { return size * 10; }
  float getBounceHeight() { return bounceHeight; }

private:
  int size;
  RGB color;
  float bounceHeight;
};

#endif // BUBBLE_H_
