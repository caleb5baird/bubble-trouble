#+title: Assets
GameStarter has build-in functionality to draw svgs. To do so you need to create a opengl-object json file and add it to [[file:opengl-objects/][assets/opengl-objects]]. This file will contain the paths, polygons, circles, and colors needed to draw the svg with openGL.
 You can creat this file from an svg using the [[file:~/random/GameStarter/Svg2OpenGlObject/][Svg2OpenGlObject submodule]] using the following steps:
1. Open the [[file:~/random/GameStarter/Svg2OpenGlObject/index.html][index.html]] file in your browser
2. Upload the svg file you want.
3. Select the point sample frequency using the input field labeled "Point every x length" for the highest sample frequency and therefore the most accurate (and expensive) drawing. You can use `1`.
4. Click `Apply`
5. Click `download`.
6. This should download a file named `data.json` which should be moved into the `assets/opengl-objects` folder.
7. The object can now be used in the game. By creating and instance of OpenGLObject and passing the name of the file as the first argument of the constructor, and then calling the `draw` method of the object.
